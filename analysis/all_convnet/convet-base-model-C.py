import numpy as np
import keras
from keras.datasets import cifar10
from keras.models import Sequential, load_model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import *
from keras.metrics import *

# Setting callback report & save best report
conv_name = "Convnet-Model-C_96-kernel(3*3)-relu_96-kernel(3*3)-relu-stride-2-MaxPool(3*3)_192-kernel(3*3)-relu_192-kernel(3*3)-relu-stride-2_MaxPool(3*3)_192-kernel(3*3)_192-kernel(1*1)_10-kernel(1*1)_global-average-pool_10-softmax"
logdir = "/home/dim/models-logs/convnet/" # A changer vers ton repo de log pour tensorboard
tensorboard_callback = TensorBoard(logdir + conv_name, write_graph=True)
checkpoint = ModelCheckpoint('best-model-Conv-base-model-C.h5', monitor='val_loss', verbose=0, save_best_only= True, mode='auto')

# import CIFAR-10 dataset
(x_train, y_train),(x_test, y_test) = cifar10.load_data()

y_train = keras.utils.to_categorical(y_train, 10)
y_test = keras.utils.to_categorical(y_test, 10)

x_train = x_train.reshape(50000, 32, 32, 3)
x_test = x_test.reshape(10000, 32, 32, 3)

# model build

model = Sequential()

model.add(Conv2D(96, (3, 3), activation='relu', padding='same', input_shape=(32, 32, 3)))

model.add(Conv2D(96, (3, 3), activation='relu', padding='same' , strides = 2))
model.add(MaxPool2D(pool_size=(3, 3)))
model.add(Dropout(0.25))


model.add(Conv2D(192, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(192, (3, 3), activation='relu', padding='same', strides=2))
model.add(MaxPool2D(pool_size=(3, 3)))
model.add(Dropout(0.25))

model.add(Conv2D(192, (3, 3), activation='relu', padding='same'))

model.add(Conv2D(192, (1, 1), activation='relu', padding='valid'))

model.add(Conv2D(10, (1, 1), activation='relu', padding='valid'))

model.add(GlobalAveragePooling2D())

model.add(Dense(10, activation='softmax'))


# Print model architecture

model.summary()

model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0001), metrics= [categorical_accuracy])

model.fit(x_train, y_train, batch_size=128, epochs=100, validation_data= (x_test, y_test), callbacks=[tensorboard_callback, checkpoint])


# Evaluate the model
scores = model.evaluate(x_test, y_test, verbose = 0)
print('Final accuracy:', str(scores[1] * 100), '%')
