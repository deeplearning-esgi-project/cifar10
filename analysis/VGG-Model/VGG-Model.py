import numpy as np
import keras
from keras.datasets import cifar10
from keras.models import Sequential, load_model
from keras.optimizers import sgd
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import *
from keras.metrics import *

#For stochastic gradient descent learning algorithm
learning_rate = 0.001
momemtum = 0.005
decay = 0.0
nosterov_momemtum = False


# Setting callback report & save best report
conv_name = "VGG-Model"
logdir = "/home/dim/models-logs/convnet/" # A changer vers ton repo de log pour tensorboard
tensorboard_callback = TensorBoard(logdir + conv_name, write_graph=True)
checkpoint = ModelCheckpoint('best-model-VGG-Model.h5', monitor='val_loss', verbose=0, save_best_only= True, mode='auto')

# import CIFAR-10 dataset
(x_train, y_train),(x_test, y_test) = cifar10.load_data()

y_train = keras.utils.to_categorical(y_train, 10)
y_test = keras.utils.to_categorical(y_test, 10)

x_train = x_train.reshape(50000, 32, 32, 3)
x_test = x_test.reshape(10000, 32, 32, 3)


# model build
model = Sequential()

model.add(Conv2D(64, (3, 3), strides=1, padding='same', input_shape=(32, 32, 3), activation='relu'))
model.add(Dropout(0.15))
model.add(Conv2D(64, (3, 3), strides=1, padding='same', activation='relu'))

model.add(MaxPool2D(pool_size=(2, 2), strides=2, padding='valid'))

model.add(Conv2D(128, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Conv2D(128, (3, 3), strides=1, padding='same', activation='relu'))

model.add(MaxPool2D(pool_size=(2, 2), strides=2, padding='valid'))

model.add(Conv2D(256, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Dropout(0.25))
model.add(Conv2D(256, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Conv2D(256, (3, 3),strides=1, padding='same', activation='relu'))

model.add(MaxPool2D(pool_size=(2, 2), strides=2, padding='valid'))

model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))

model.add(MaxPool2D(pool_size=(2, 2), strides=2, padding='valid'))

model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Dropout(0.25))
model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))
model.add(Dropout(0.25))
model.add(Conv2D(512, (3, 3),strides=1, padding='same', activation='relu'))

model.add(MaxPool2D(pool_size=(2, 2), strides=2, padding='valid'))

model.add(Flatten())

model.add(Dropout(0.25))
model.add(Dense(4096, activation='relu'))
model.add(Dense(406, activation='relu'))
model.add(Dense(10, activation='softmax'))


# Print model architecture
model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=sgd(lr=learning_rate,
                             momentum=momemtum,
                             decay = decay,
                             nesterov=nosterov_momemtum),
              metrics= [categorical_accuracy])


model.fit(x_train, y_train, batch_size=32, epochs=100, validation_data= (x_test, y_test), callbacks=[tensorboard_callback, checkpoint])


# Evaluate the model
scores = model.evaluate(x_test, y_test, verbose = 0)
print('Final accuracy:', str(scores[1] * 100), '%')



