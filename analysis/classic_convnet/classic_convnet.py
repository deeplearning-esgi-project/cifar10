import numpy as np
import keras
from keras.datasets import cifar10
from keras.models import Sequential, load_model
from keras.optimizers import Adam,SGD
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.layers import *
from keras.metrics import *

batch_size = 32
#For Stocastic Gradient Descent
learning_rate = 0.1
momemtum=0.9

# Setting callback report & save best report
#"Convnet-32-kernel-3*3_32-kernel-3*3_Pool-2*2_Dropout0.25-Flatten-Full-connected-512=relu_Dropout0.5_FC-10=softmax[SGD-crossentropy]"
conv_name = "BaseConvolutional-SGD-lr" + str(learning_rate)
conv_name += "-momemtum"
conv_name += str(momemtum)
conv_name += "-batch_size" + str(batch_size)
tensorboard_callback = TensorBoard("/home/dim/models-logs/convnet/" + conv_name + "_32-(3*3)_32-(3*3)_pool_64-(3*3)_512_10", write_graph=True)
checkpoint = ModelCheckpoint('best-model-simple.h5', monitor='val_loss', verbose=0, save_best_only= True, mode='auto')

# import CIFAR-10 dataset
(x_train, y_train),(x_test, y_test) = cifar10.load_data()

y_train = keras.utils.to_categorical(y_train, 10)
y_test = keras.utils.to_categorical(y_test, 10)


# model build

model = Sequential()

model.add(Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(32,32, 3)))
model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(512, activation='relu'))
model.add(Dropout(0.5))

model.add(Dense(10, activation='softmax'))


sgd = SGD(lr=learning_rate, momentum=momemtum,nesterov = True)

model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics= [categorical_accuracy])

# Print model architecture
model.summary()

# Train model
model.fit(x_train, y_train, batch_size=batch_size, epochs=100, validation_data= (x_test, y_test), callbacks=[tensorboard_callback, checkpoint])



# Evaluate the model
scores = model.evaluate(x_test, y_test, verbose = 0)
print('Final accuracy:', str(scores[1] * 100), '%')

# Model results analysis
#prediction = model.predict(x_test, batch_size=32)
#label_pred = np.argmax(prediction, axis=1)

#correct = (label_pred == y_test)
#print('Number of correct classification : ', str(sum(correct)))


# Show mis-classifications
#incorrect = (correct == False)

# get image with wrong classification
#mis_image = x_test[incorrect]

# get wrong prediction for those images
#labels_err = label_pred[incorrect]

# get the true label for those images
#labels_true = y_test[incorrect]

#from matplotlib import pyplot as plt

#def plot_images(images, labels_true, class_names, labels_pred=None):

    #Create a figure with sub-plots
 #   fig, axes = plt.subplots(3, 3, figsize = (8, 8))

    #Adjust the vertical spacing
   # if label_pred is None:
   #     hspace = 0.2
   # else:
  #      hspace = 0.5

  #  fig.subplots_adjust(hspace = hspace, wspace = 0.3)

  #  for i, ax in enumerate(axes.flat):

        #for now don't crash when less than 9 images
      #  if i < len(images):
            #Plot the image
          #  ax.imshow(images[i], interpolation='spline16')

            #Name of the correct class
          #  labels_true_name = class_names[labels_true[i]]

            # Show true and predicted classes
         #   if labels_pred in None:
       #             xlabel = "Correct: " + labels_true_name
#            else:
                # Name of predicted class
#                labels_pred_name = class_names[labels_pred[i]]
#                xlabel = "Correct: " + labels_true_name + "\nPredicted : " + labels_pred_name

            #Show the class on the x axis
#            ax.set_xlabel(xlabel)

        # Remove ticks from the plot
#        ax.set_xticks([])
#        ax.set_yticks([])

    # Show the plot
 #   plt.show()





